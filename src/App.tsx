import React from 'react';
import { Provider } from 'react-redux';
import { store } from './store';
import styled from 'styled-components';
import './App.css';

const mediaDevices = navigator.mediaDevices as any;
export class App extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      isRecord: false,
    };
  }
  handleRecord = () => {
    const { desktopCapturer } = require('electron');
    desktopCapturer.getSources({ types: ['window', 'screen'] }).then(async (sources: any) => {
      for (const source of sources) {
        if (source.name === 'Entire Screen' || source.name === 'Screen 1') {
          try {
            let recorder = await mediaDevices.getUserMedia({
              audio: false,
              video: {
                mandatory: {
                  chromeMediaSource: 'desktop',
                  chromeMediaSourceId: source.id,
                  minWidth: 1280,
                  maxWidth: 1280,
                  minHeight: 720,
                  maxHeight: 720,
                },
              },
            });
            this.handleStream(recorder);
          } catch (e) {
            console.log(e);
          }
          return;
        }
      }
    });
  };

  handleStream = (stream: any) => {
    this.setState({ isRecord: true });
    const video: any = document.querySelector('video');
    video.srcObject = stream;
    video.onloadedmetadata = () => {
      video.play();
    };
  };

  handleStop = () => {
    this.setState({ isRecord: false });
    const video: any = document.querySelector('video');
    video.srcObject = null;
  };

  render() {
    return (
      <Provider store={store}>
        <div className="app">
          <div className="btnDiv">
            <StyleBtnStart onClick={this.handleRecord} className="btnStart" disabled={this.state.isRecord}>
              Start
            </StyleBtnStart>
            <StyleBtnStop onClick={this.handleStop} className="btnStop" disabled={!this.state.isRecord}>
              Stop
            </StyleBtnStop>
          </div>
          <div className="divVideo">
            <div className="title">Record Video</div>
            <video className="videoView"></video>
          </div>
        </div>
      </Provider>
    );
  }
}

const StyleBtnStart = styled.button`
  :hover {
    background: ${(props) => (props.disabled ? 'rgb(156, 167, 166)' : 'rgb(130, 245, 239)')};
  }
`;

const StyleBtnStop = styled.button`
  :hover {
    background: ${(props) => (!props.disabled ? 'rgb(130, 245, 239)' : 'rgb(156, 167, 166) ')};
  }
`;
